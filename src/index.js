import App from "./App"
import React from "react"
import ReactDOM from "react-dom"
import './styles.scss'

global.isClient = typeof window !== undefined

const wrapper = document.getElementById( "devbase-app" )
ReactDOM.render( <App />, wrapper )
