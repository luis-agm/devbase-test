import React, {useState} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import { Content } from './components/styled'
import Details from './pages/Details'
import UserList from './pages/UserList'
import { CurrentUserProvider } from './userContext'


const App = ( ) => {
  return (
    <div className='App'>
      <CurrentUserProvider>
        <Switch>
          <Route exact path='/' component={UserList} />
          <Route path='/details' component={Details} />
        </Switch>
      </CurrentUserProvider>
    </div>
  )
}

const RoutedApp = () =>  ( 
  <Router>
    <App />
  </Router> 
)



export default RoutedApp