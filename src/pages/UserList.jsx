import React, { useState, useEffect, useContext } from 'react'
import NavBar from '../components/NavBar'
import { List, Button, Content } from '../components/styled'
import { Link, useHistory } from 'react-router-dom'
import CurrentUserContext from '../userContext'

const UserList = () => {
  const users = ["GrahamCampbell","fabpot","weierophinney","rkh","josh"]

  const {setCurrentUser, clearCurrentUser} =  useContext( CurrentUserContext )

  const history = useHistory()

  const clickHandler = ( evt ) => {
    clearCurrentUser()
    setCurrentUser( evt.target.id )
    history.push( "/details" )
  }

  useEffect( () => {
    clearCurrentUser()
  },
  []
  )

  return (
    <>
      <NavBar title='Home' />
      <Content>
        <h2>Top 5 GitHub Users</h2>
        <p>Tap the username to see more information</p>
        <List>
          {users.map( ( user ) => {
            return (
              <Button key={user} id={user} onClick={clickHandler}>
                {user}
              </Button>
            )
          } 
          )}
        </List>
      </Content>
    </>
  )
}

export default UserList
