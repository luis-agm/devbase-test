import React, {useState, useContext, useEffect} from 'react'
import NavBar from '../components/NavBar'
import { Content } from '../components/styled'
import axios from 'axios'
import CurrentUserContext from '../userContext'

const Details = ( {user} ) => {
  const [userDetails, setUserDetails] = useState( null )
  const { currentUser } = useContext( CurrentUserContext )

  useEffect( () => {
    axios.get( `https://api.github.com/users/${currentUser}` )
    .then( ( {data: userData} ) => {
      setUserDetails( userData )
    }
    )
  },
  [currentUser]
  )

  return (
    <>
      <NavBar title='Details' goback />
      <Content>
        {userDetails &&
          <>
            <img src={userDetails.avatar_url} />
            <p>{userDetails.name}</p>
            <p>{userDetails.location}</p>
          </>}
      </Content>
    </>
  )
}

export default Details
