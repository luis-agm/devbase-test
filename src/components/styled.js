import styled from 'styled-components'

export const Content = styled.div`
  width: 100%;
  padding: 30px;
  font-size: 1.5rem;
`

export const Nav = styled.nav`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  height: 5vh;
  width: 100%;
  background: black;
  color: white;
  font-size: 2rem;
  padding: 10px;

  span {
    position: absolute;
    left: 15px;
    top: 50%;
    transform: translateY(-50%);
    font-size: 2rem;
  }
`

export const Button = styled.button`
  height: 5vh;
  background: skyblue;
  padding: 0 5%;
  color: white;
  font-size: 2.5rem;
  border-radius: 6px;
  border: none;
`

export const List = styled.ul`
  width: 100%;
  padding: 0;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;

  > * {
    margin-bottom: 5px;
    margin-right: 5px;
  }
`

export const ListItem = styled.li`
`