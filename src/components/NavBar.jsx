import React from 'react'
import { Nav } from './styled'
import { Link } from 'react-router-dom'

function NavBar( {title, goback} ) {
  return (
    <Nav>
      {goback &&
        <Link className='navbar__link' to='/'> {"< Back"} </Link>}
      <h1 className='navbar__title'>{title}</h1>
    </Nav>
  )
}

export default NavBar
