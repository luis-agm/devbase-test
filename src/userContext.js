import React, { useState } from 'react'

const CurrentUserContext = React.createContext( null )

const { Provider } = CurrentUserContext

export const CurrentUserProvider = ( { children } ) => {
  const [currentUser, setCurrentUser] = useState( null )

  const clearCurrentUser = () => setCurrentUser( null )
 
  const state = {
    currentUser,
    setCurrentUser,
    clearCurrentUser
  }

  return (
    <Provider value={state}>
      {children}
    </Provider>
  )
}

export default CurrentUserContext